Symfony oauth REST api
=======

### The following bundles are used :
- https://github.com/FriendsOfSymfony/FOSRestBundle
- https://github.com/FriendsOfSymfony/FOSUserBundle
- https://github.com/FriendsOfSymfony/FOSOAuthServerBundle
- https://github.com/nelmio/NelmioApiDocBundle
- https://github.com/nelmio/alice


http util see [HTTPie library](https://github.com/jakubroztocil/httpie)

### Get access token:
```bash
    http POST http://127.0.0.1:8000/oauth/v2/token grant_type=password client_id=4_1j6czm5f4h0k0g8skks4g0cc44kcsog4k08o4c4kc4sgkk04s8 client_secret=5d6umy7fgmos4g0cc88og8o8ocgosgk4kcscswwgggg0k4ws44 username=admin password=admin
```
where client_id and client_secret see in oauth2_clients table

### Use api
```bash 
    http GET http://127.0.0.1:8000/states "Authorization:Bearer MzRjN2RhOTI3YTZjOWQ5MjMwODhiYmRlMDhhNTUyYjZiNzAwNTQwZmZlZDc5NDQ3OTVlYmI1NmM0NGRlMmI0Yg"
```

### Add to end Authorization:Bearer params:
```bash
    http GET http://127.0.0.1:8000/users "Authorization:Bearer MzRjN2RhOTI3YTZjOWQ5MjMwODhiYmRlMDhhNTUyYjZiNzAwNTQwZmZlZDc5NDQ3OTVlYmI1NmM0NGRlMmI0Yg"
```
```bash
    http GET http://127.0.0.1:8000/users/207/show "Authorization:Bearer MzRjN2RhOTI3YTZjOWQ5MjMwODhiYmRlMDhhNTUyYjZiNzAwNTQwZmZlZDc5NDQ3OTVlYmI1NmM0NGRlMmI0Yg"
```
```bash
    http POST http://127.0.0.1:8000/users/create username="Test User1" email=test1@mail.ru password=qwe "Authorization:Bearer MzRjN2RhOTI3YTZjOWQ5MjMwODhiYmRlMDhhNTUyYjZiNzAwNTQwZmZlZDc5NDQ3OTVlYmI1NmM0NGRlMmI0Yg"
```
```bash
    http PUT http://127.0.0.1:8000/users/218/update username="Test User1" "Authorization:Bearer MzRjN2RhOTI3YTZjOWQ5MjMwODhiYmRlMDhhNTUyYjZiNzAwNTQwZmZlZDc5NDQ3OTVlYmI1NmM0NGRlMmI0Yg"
```
```bash
    http DELETE http://127.0.0.1:8000/users/206/delete "Authorization:Bearer MzRjN2RhOTI3YTZjOWQ5MjMwODhiYmRlMDhhNTUyYjZiNzAwNTQwZmZlZDc5NDQ3OTVlYmI1NmM0NGRlMmI0Yg"
```
