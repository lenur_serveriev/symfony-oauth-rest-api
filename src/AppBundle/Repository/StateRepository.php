<?php

namespace AppBundle\Repository;

use AppBundle\Entity\State;
use Doctrine\ORM\EntityRepository;

class StateRepository extends EntityRepository
{
	/**
	 * @param $state
	 */
	public function delete(State $state)
	{
		$this->getEntityManager()->remove($state);
		$this->getEntityManager()->flush();
	}

	/**
	 * Stores User in repository
	 *
	 * @param State $state
	 */
	public function persist(State $state)
	{
		$this->getEntityManager()->persist($state);
		$this->getEntityManager()->flush();
	}
}
