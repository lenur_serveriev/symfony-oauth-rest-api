<?php
namespace AppBundle\Controller;

use AppBundle\Entity\State;
use AppBundle\Form\StateType;
use AppBundle\Repository\StateRepository;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View as FOSView;

/**
 * @Rest\RouteResource("State")
 */
class StatesController extends BaseController
{
    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Get the list of state and filtered by params",
     *  statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @Rest\QueryParam(name="order_by", nullable=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @Rest\QueryParam(name="filters", nullable=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     * @Rest\Get("/states", name="api_states")
     * @Rest\View()
     *
     * @param ParamFetcherInterface $param_fetcher
     * @return array
     * @internal param Request $request the request object
     */
    public function getStatesAction(ParamFetcherInterface $param_fetcher)
    {
	    try {
		    $offset   = $param_fetcher->get('offset');
		    $limit    = $param_fetcher->get('limit');
		    $order_by = $param_fetcher->get('order_by');
		    $filters  = !is_null($param_fetcher->get('filters')) ? $param_fetcher->get('filters') : [];

		    $states = $this->getRepo()->findBy($filters, $order_by, $limit, $offset);
		    if ($states) {
			    return $this->getSerializeData($states);
		    }

		    return $this->handleView($this->view('Not Found', Response::HTTP_NO_CONTENT));
	    } catch (\Exception $e) {
		    return $this->handleView($this->view($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR));
	    }
    }

    /**
     * @ApiDoc(
     *  resource = true,
     *  description = "Gets a state for a given id",
     *  output = "AppBundle\Entity\State",
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      404 = "Returned when the page is not found"
     *  }
     * )
     * @Rest\Get("/states/{id}/show", name="api_states_show")
     * @Rest\View()
     *
     * @param int $stateId state id
     * @return \AppBundle\Entity\State
     */
    public function getAction(State $state)
    {
	    return $this->getSerializeData($state);
    }

	/**
	 * Create a state from the submitted data.
	 *
	 * @ApiDoc(
	 *   resource = true,
	 *   description = "Creates a new state from the submitted data.",
	 *   input = "AppBundle\Form\StateType",
	 *   statusCodes = {
	 *     200 = "Returned when successful",
	 *     400 = "Returned when the form has errors"
	 *   }
	 * )
	 *
	 * @Rest\Post("/states/create", name="api_states_create")
	 *
	 * @param Request $request
	 * @return State|FOSView|static
	 */
	public function createAction(Request $request)
	{
		$state = new State();
		$form = $this->createForm(new StateType(), $state, ["method" => $request->getMethod()]);
		$form->handleRequest($request);
		
		if ($form->isValid()) {
			$this->getRepo()->persist($state);

			return $this->getSerializeData($state);
		}

		return FOSView::create(['errors' => $form->getErrors()], Response::HTTP_INTERNAL_SERVER_ERROR);
	}

	/**
	 * Update existing state from the submitted data or create a new state at a specific location.
	 *
	 * @ApiDoc(
	 *   resource = true,
	 *   input = "AppBundle\Form\StateType",
	 *   statusCodes = {
	 *     201 = "Returned when the state is created",
	 *     204 = "Returned when successful",
	 *     400 = "Returned when the form has errors"
	 * })
	 *
	 * @Rest\Put("/states/{id}/update", name="api_states_update")
	 * @param Request $request
	 * @param $state
	 *
	 * @return Response
	 */
	public function putAction(Request $request, State $state)
	{
		try {
			$request->setMethod('PATCH');
			$form = $this->createForm(new StateType(), $state, ["method" => $request->getMethod()]);

			$form->handleRequest($request);

			if ($form->isValid()) {
				$this->getDoctrine()->getManager()->flush($state);

				return $this->getSerializeData($state);
			}

			return FOSView::create(['errors' => $form->getErrors()], Response::HTTP_INTERNAL_SERVER_ERROR);
		} catch (\Exception $e) {
			return FOSView::create($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

    /**
     * Delete a state.
     *
     * @ApiDoc(
     * resource = true,
     * statusCodes={
     *      204="Returned when successful"
     *  }
     * )
     *
     * @Rest\Delete("/states/{id}/delete", name="api_users_delete")
     *
     * @param State $state
     *
     * @return FOSView|static
     */
    public function deleteAction(State $state)
    {
	    try {
		    $this->getRepo()->delete($state);

		    return FOSView::create(null, Response::HTTP_OK);
	    } catch (\Exception $e) {
		    return FOSView::create($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
	    }
    }

    /**
     * @return StateRepository
     */
    public function getRepo()
    {
	    return $this->getDoctrine()->getRepository('AppBundle:State');
    }
}